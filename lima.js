function Digits(N) {
    return (/([0-9]).*?\1/).test(N) //Ekspresi [0-9] digunakan untuk menemukan karakter apa pun yang BUKAN digit.
  }  
  
  console.log(
  [5,555].map(Digits)
  )